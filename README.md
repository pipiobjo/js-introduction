# js-introduction

## Day One

* Singelpage (Issam)
  * Theory - Advantages
  * Architektur
* nodejs and npm (Issam)
  * IDE for development (like MS Visual Studio Code , a electron sample, also on https://github.com/Microsoft/vscode)
  * npm install --save-dev vs --save
  * package.js und package-lock.js
  * node_modules
  * module Patching (node_modules)

* TypeScript vs ECMAScript 6 vs ES5 (Issam)
  * import { xy } vs import xy from
  * funktio = () => {} vs funktio = () => ()
  * TypeScript samples
  * linting, eslint

* MVC in Vuejs vs Angular (Issam)
  * Component State and globales state
  * virtual dom: dom manipulation mit mehreren frameworks gleichzeitig (ng oder vuejs oder react) ist ohne weiteres nicht möglich.

* html webcomponents (Björn)

* Stylequides (Björn)
  * Atomic Design
    * Where to spilt artefacts?
    * How to prevent from to big / complex to change?
    * Who ownes the patterns?
    * General theme vs Component theme?
* Architektur Diskussion <Issam + Björn />

## Day Two

* angular vs vuejs <Issam
  * compenent passing paramet
  * work with state
  * work with store (vuex vs ngrx(redux-store))
  * passing value from input to click button
  * form validation / action-request
  * reading url paramter
  * rest-calls
  * Testing (nightwatch)
  * test:unit
  * debuging: source-map

* Liferay
  * ng as liferay portlet
  * vuejs as liferay portlet
  * Interaktion zwischen mehrere Applikationen bzw. Portlet (ng to vuejs)
  * Use of Liferay JS - API via window.liferay
  * Using CSS from Theme
  * Overwrite CSS from Theme
  * Mandantenbasiertes CSS

* Apps
  * ng as app
  * vuejs as app
  * Apache http Server
    * ng from apache
    * vue from apache />

## Day Three

* Statistics (vuejs VS angular, vs react) (Björn)
  * https://npm-stat.com/charts.html?package=react&package=vue&package=%40angular%2Fcore&from=2014-12-12&to=2018-12-12
  * stars:
    * https://github.com/vuejs/vue/pulse
    * https://github.com/facebook/react
    * https://github.com/angular/angular
  * [google search stats:]( https://trends.google.com/trends/explore?date=today%205-y,today%205-y,today%205-y&geo=,,&q=React.js,Vue.js,%2Fm%2F0j45p7w#TIMESERIES)
  * [StackOverflow:](https://insights.stackoverflow.com/trends?utm_source=so-owned&utm_medium=blog&utm_campaign=trends&utm_content=blog-link&tags=angularjs%2Creactjs%2Cvue.js)

* Tabelle: mit Vergleich der Beiden Framworks (Issam)

* Liferay: (Björn)
  * Workflow from JS-App to Portlet
    * create App
    * push to npm-registry
    * Use a framwork inside (styleguide) -> ** use button from styleguide
    * Use JS-APP in Portlet
    * Publish JAX-RS Services
    * Consume JAX-RS Service via JS-App
    * Use JAX-RS Services during development
    * Stage configuration (dev, prod) />
