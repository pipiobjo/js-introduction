# Vue Styleguidist basic example style guide

How to start locally:

```
npm install
npm run styleguide
```

Then open [http://localhost:6060](http://localhost:6060) in your browser.




# Atomic Design -> Design Systems

Atoms
Molecules
Organisms
Templates
Pages

# Styleguide Discussions

Where to spilt artefacts? 
How to prevent from to big / complex to change?
Who ownes the patterns?
General theme vs Component theme?


https://www.creativebloq.com/web-design/10-reasons-you-should-be-using-atomic-design-61620771
https://www.designbetter.co/design-systems-handbook/introducing-design-systems

