# Four Pillars of Web Components

The HTML and DOM standards define four new standards/APIs that are helpful for defining Web Components. These standards are:

[Custom Elements:](https://www.w3.org/TR/custom-elements/)

    With Custom Elements, web developers can create new HTML tags, beef-up existing HTML tags, or extend the components other developers have authored. This API is the foundation of Web Components.

[HTML Templates:](https://www.html5rocks.com/en/tutorials/webcomponents/template/#toc-pillars)

    It defines a new <template> element, which describes a standard DOM-based approach for client-side templating. Templates allow you to declare fragments of markup which are parsed as HTML, go unused at page load, but can be instantiated later on at runtime.
    [Further informations](https://www.html5rocks.com/en/tutorials/webcomponents/template/) [Slots](https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_templates_and_slots)

[Shadow DOM:](https://dom.spec.whatwg.org/#shadow-trees)

    Shadow DOM is designed as a tool for building component-based apps. It brings solutions for common problems in web development. It allows you to isolate DOM for the component and scope, and simplify CSS, etc.

[HTML Imports](https://www.html5rocks.com/en/tutorials/webcomponents/imports/):

    While HTML templates allow you to create new templates, HTML imports allows you to import these templates from different HTML files. Imports help keep code more organized by neatly arranging your components as separate HTML files. [Sample and deeper informations](https://www.html5rocks.com/en/tutorials/webcomponents/imports/)

[Source](https://www.codementor.io/ayushgupta/vanilla-js-web-components-chguq8goz)


# Execution

```bash

npm install
npm run start

```

Open browser http://localhost:3000