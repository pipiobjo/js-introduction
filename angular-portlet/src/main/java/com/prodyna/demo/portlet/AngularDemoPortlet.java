package com.prodyna.demo.portlet;

import com.prodyna.demo.constants.AngularDemoPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author ILamani
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Angular Example",
		"com.liferay.portlet.header-portlet-css=/css/styles.css",
		"com.liferay.portlet.header-portlet-javascript=/js/runtime.js",
		"com.liferay.portlet.header-portlet-javascript=/js/es2015-polyfills.js",
		"com.liferay.portlet.header-portlet-javascript=/js/polyfills.js",
		"com.liferay.portlet.header-portlet-javascript=/js/main.js",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.html",
		"javax.portlet.init-param.valid-paths=/view.html",
		"javax.portlet.name=" + AngularDemoPortletKeys.AngularDemo,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class AngularDemoPortlet extends MVCPortlet {
}