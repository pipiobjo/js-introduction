
Start it via:

```bash
docker-compose up -d 		# -d - daemon mode

```

Stop

```bash
docker-compose down
```

Access:

http://localhost:4873/

Users: 
* admin:admin
* jpicado:jpicado


Adding User
Generate Entry via http://www.htaccesstools.com/htpasswd-generator/ -> htpasswd


Configure npm:

Use it as mirror: 

```bash
npm config set registry http://localhost:4873/

revert it via
npm config set registry https://registry.npmjs.org/

```

Auth npm against it to use it to publish packages

```bash
npm adduser --registry http://localhost:4873
```

Configuration changes are persisted in ~/.npmrc


Offical Website
https://verdaccio.org/en/




