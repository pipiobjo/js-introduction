package com.pipiobjo.application.model;

public class Post {

	private String title;
	
	public Post(String title) {
		this.title = title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getTitle() {
		return this.title;
	}
}
