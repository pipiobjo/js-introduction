package com.pipiobjo.application;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.osgi.service.component.annotations.Component;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component(
        immediate = true,
        property = {
                "servlet-context-name=",
                "servlet-filter-name=Custom Filter",
                "url-pattern=/*"
        },
        service = Filter.class
)
public class CorsFilter extends BaseFilter {
    private static final Log _log = LogFactoryUtil.getLog(CorsFilter.class);
    private static final Logger logger = LoggerFactory.getLogger(CorsFilter.class);
    @Override
    protected void processFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws Exception {

        String requestOrigin = request.getHeader("origin");
        String accessControlHeader = request.getHeader("access-control-request-headers");
        if(accessControlHeader!=null){
            Set<String> headers = new HashSet<String>(Arrays.asList(accessControlHeader.split(",")));
            // check cors preflight call
            if("http://localhost:9000".equalsIgnoreCase(requestOrigin) && headers.contains("authorization")){
                logger.info("Allow cors preflight call");
                response.addHeader("Access-Control-Allow-Credentials", Boolean.TRUE.toString());
                response.addHeader("Access-Control-Allow-Origin", requestOrigin);
                response.addHeader("Access-Control-Allow-Headers", accessControlHeader);
                response.setStatus(200);
                return;
            }
        }

        if("http://localhost:9000".equalsIgnoreCase(requestOrigin)){
            logger.info("Allow cors call");
            response.addHeader("Access-Control-Allow-Origin", requestOrigin);
        }
        super.processFilter(request, response, filterChain);
    }

    @Override
    protected Log getLog() {
        return _log;
    }


}