package com.pipiobjo.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;

import com.pipiobjo.application.restResources.Greetings;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.ConfigurationPolicy;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.pipiobjo.application.model.Post;

import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

/**
 * @author pipiobjo
 */
@Component(
		name= "com.pipiobjo.application.MyRestServiceApplication",
		immediate = true,
		configurationPid = "com.liferay.portal.security.auth.verifier.internal.basic.auth.header.module.configuration.BasicAuthHeaderAuthVerifierConfiguration",
		configurationPolicy = ConfigurationPolicy.OPTIONAL,
		property = {

				JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE + "=/pipiobjo",
				JaxrsWhiteboardConstants.JAX_RS_NAME + "=Pipiobjo.Rest",
		},
		service = Application.class
)

public class MyRestServiceApplication extends Application {
	Logger logger = LoggerFactory.getLogger(MyRestServiceApplication.class);

	@Reference
	private volatile UserLocalService userLocalService;


	@Override
	public Set<Object> getSingletons() {
		logger.info("MyRestServiceApplication instanciate singletons");
		Set<Object> singeltons = new HashSet<>(Arrays.asList(new JacksonJsonProvider(), this));
		singeltons.add(this);

//		singeltons.add( greetingsResource);
		singeltons = Collections.unmodifiableSet(singeltons);
		return singeltons;

	}


	@GET
	@Path("/morning")
	@Produces("application/json;charset=UTF-8")
	public String hello() {
		return "Good morning bjoern!";
	}

	@Path("/greetings")
	@GET()
	@Produces("application/json;charset=UTF-8")
	public List<Post> working(@Context HttpServletRequest httpRequest, @Context HttpServletResponse httpResponse) throws NumberFormatException, PortalException {

		List<Post> result = new ArrayList<>();
		String remoteUser = httpRequest.getRemoteUser();

		User user = userLocalService.getUserById((Long.parseLong(remoteUser)));
		logger.info("remoteUser={} and screenName={}", remoteUser, user.getScreenName());
		if(user.isDefaultUser()) {
			result.add(new Post("Unauthenticated"));
		}else {
			result.add(new Post("It works!"));
			result.add(new Post("Title 1"));
			result.add(new Post("screenName=" + user.getScreenName()));
		}

		return result;
	}

	@Activate
	public void activate() {
		logger.info("restserviceapp activated");
	}



}
