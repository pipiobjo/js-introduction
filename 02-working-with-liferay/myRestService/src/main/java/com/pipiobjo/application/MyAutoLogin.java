package com.pipiobjo.application;
import com.liferay.portal.kernel.model.Company;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auto.login.AutoLogin;
import com.liferay.portal.kernel.security.auto.login.AutoLoginException;
import com.liferay.portal.kernel.security.auto.login.BaseAutoLogin;
import com.liferay.portal.kernel.service.CompanyLocalService;
import com.liferay.portal.kernel.service.UserLocalService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component(immediate = true, service = AutoLogin.class)
public class MyAutoLogin extends BaseAutoLogin {
	
	Logger logger = LoggerFactory.getLogger(MyAutoLogin.class);
	
	@Reference
	private UserLocalService userLocalService;
	
	@Reference
	private CompanyLocalService companyLocalService;


	@Override
	protected String[] doLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
	  	String[] credentials = null;

		logger.info("\n\n CALLING AUTO LOGIN HOOK \n \n");
//			User autoLoginUser = null;
//
//			try {
//				Company companyByWebId = companyLocalService.getCompanyByWebId("liferay.com");
//				autoLoginUser = userLocalService.getUserByScreenName(companyByWebId.getCompanyId(), "test@liferay.com");
//			}
//			catch (Exception e) {
//			}
//
//			if (autoLoginUser != null) {
//				credentials = new String[3];
//
//				credentials[0] = String.valueOf(autoLoginUser.getUserId());
//				credentials[1] = autoLoginUser.getPassword();
//				credentials[2] = Boolean.toString(true);
//			}
//
//			if (logger.isInfoEnabled()) {
//				logger.info(
//					"Logged in as" + autoLoginUser.getFullName() +
//						"by Blade Auto Login");
//			}

	return credentials;
	}

}