 #!/bin/bash

echo -e "\n \n \n CHECK com.liferay.frontend.js.portlet.extender-1.0.6 \n \n"
WEBCONSOLE_FILE="/opt/liferay/osgi/modules/com.liferay.frontend.js.portlet.extender-1.0.6.jar"
if [ ! -f $WEBCONSOLE_FILE ]; then
    echo "download com.liferay.frontend.js.portlet.extender-1.0.6"
    curl -s http://central.maven.org/maven2/com/liferay/com.liferay.frontend.js.portlet.extender/1.0.6/com.liferay.frontend.js.portlet.extender-1.0.6.jar --output $WEBCONSOLE_FILE
fi

