 #!/bin/bash

echo -e "\n \n \n CHECK APACHE FELIX WEBCONSOLE \n \n"
WEBCONSOLE_FILE="/opt/liferay/osgi/modules/org.apache.felix.webconsole-4.3.8-all.jar"
if [ ! -f $WEBCONSOLE_FILE ]; then
    echo "download apache felix webconsole"
    curl -s https://www-eu.apache.org/dist/felix/org.apache.felix.webconsole-4.3.8-all.jar --output $WEBCONSOLE_FILE
fi

