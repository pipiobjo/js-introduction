/**
 * This is the main entry point of the portlet.
 *
 * See https://tinyurl.com/js-ext-portlet-entry-point for the most recent 
 * information on the signature of this function.
 *
 * @param  {Object} params a hash with values of interest to the portlet
 * @return {void}
 */
// import '@pipiobjo/ui-app/dist/js/about.ccb32d41'
// import '@pipiobjo/ui-app/dist/js/app.b19f1a62'
// import '@pipiobjo/ui-app/dist/js/chunk-vendors.dcfd4564'
import '@js-based-portlet$pipiobjo/ui-app@0.1.1/dist/css/app.c5fac1db.css'
export default function main({portletNamespace, contextPath, portletElementId, configuration}) {
    
    const node = document.getElementById(portletElementId);
    node.innerHTML = `

    <link href=/o/js/resolved-module/@js-based-portlet$pipiobjo/ui-app@0.1.1/dist/css/app.c5fac1db.css rel="stylesheet" type="text/css">

    <div id="app"></div>

    `;
/*
    node.innerHTML =`

    
        <div>
            <span class="tag">${Liferay.Language.get('porlet-namespace')}1:</span>
            <span class="value">${portletNamespace}</span>
        </div>
        <div>
            <span class="tag">${Liferay.Language.get('context-path')}2:</span>
            <span class="value">${contextPath}</span>
        </div>
        <div>
            <span class="tag">${Liferay.Language.get('portlet-element-id')}3:</span>
            <span class="value">${portletElementId}</span>
        </div>
        
        <div>
            <span class="tag">${Liferay.Language.get('configuration')}4:</span>
            <span class="value">
                ${JSON.stringify(configuration, null, 2)}
            </span>
        </div>
        
    `;
*/    



Liferay.Loader.require('@js-based-portlet$pipiobjo/ui-app@0.1.1/dist/js/app.b19f1a62', function(myApp) {
    console.log("app=",myApp)
}, function(error) {
    console.error(error);
});

Liferay.Loader.require('@js-based-portlet$pipiobjo/ui-app@0.1.1/dist/js/about.ccb32d41', function(myApp) {
    console.log("app=",myApp)
}, function(error) {
    console.error(error);
});

Liferay.Loader.require('@js-based-portlet$pipiobjo/ui-app@0.1.1/dist/js/chunk-vendors.dcfd4564', function(myApp) {
    console.log("app=",myApp)
}, function(error) {
    console.error(error);
});





    
}