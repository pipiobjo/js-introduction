# js-based-portlet

Applying ui-app to portlet

Bundler Docu https://github.com/liferay/liferay-npm-build-tools/wiki/How-to-use-generator-liferay-bundle

Requirements:
* Deployment of https://web.liferay.com/de/marketplace/-/mp/application/115543020
   * Old Version
* Download it via MavenCentral https://mvnrepository.com/artifact/com.liferay/com.liferay.frontend.js.portlet.extender
