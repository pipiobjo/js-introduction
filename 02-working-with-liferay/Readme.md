# Open Todos

* Use of swagger to document the JAXRS Endpoints
  * maven plugin https://github.com/openapi-tools/swagger-maven-plugin
  * Providing a endpoint to publish, only for admins
* Build portlet which uses swagger-ui
  * doc https://github.com/swagger-api/swagger-ui
  * npm https://www.npmjs.com/package/swagger-ui
* CSS Improvement
  * Support import css, build one css file, which replaces the default /asset/styles.css
* How to publish the generated jar to a maven repository
  * https://www.npmjs.com/package/maven-deploy

# Overview

Ways to make UI Development with liferay:

* Completly encapsulated, just using rest services
* Completly embedded, using portlet builds
* Mixture: encapsulate during development, embedded during runtime
  * Fast and independent ui development with webpack-dev-server
  * More complex setup

```text

    +-------+         +---------+
    |       |  CORS   |         |
    | JS UI | +-----> | Liferay |
    |       |         |         |
    +-------+         +---------+
   npm build           Portlet
   npm publish         npm install js-ui
    +                        ^
    |       +------------+   |
    +------>+npm registry+---+
            +------------+
```

Cors:
https://www.moesif.com/blog/technical/cors/Authoritative-Guide-to-CORS-Cross-Origin-Resource-Sharing-for-REST-APIs/#

# Liferay Setup Steps

- Download latest liferay-7.1 CE from https://sourceforge.net/projects/lportal/

  - Extract it to the bundle folder in futher steps called LIFERAY_HOME
  - There is already a portal-setup-wizard.properties file

- Install webconsole from
  - curl https://www-eu.apache.org/dist/felix/org.apache.felix.webconsole-4.3.8-all.jar --output ./bundles/files/deploy/org.apache.felix.webconsole-4.3.8-all.jar
  - check http://localhost:8080/o/system/console/bundles
  - uid: admin pwd: admin

## Containerized Setup

Pull images and start

```bash
docker-compose pull
docker-compose -f docker-compose.yml up -d
```

Stop and remove all containers

```bash
docker-compose stop && docker-compose rm -f
```

List all running containers

```bash
$ docker ps -a
CONTAINER ID        IMAGE                          COMMAND                  CREATED             STATUS                    PORTS                                                       NAMES
c4f6c919c4b7        verdaccio/verdaccio:4.x-next   "uid_entrypoint /bin…"   16 minutes ago      Up 16 minutes             0.0.0.0:4873->4873/tcp                                      verdaccio
2be4aebf6e96        liferay/portal:7.1.2-ga3       "/bin/sh -c /etc/lif…"   16 minutes ago      Up 16 minutes (healthy)   0.0.0.0:8000->8000/tcp, 0.0.0.0:8080->8080/tcp, 11311/tcp   liferay
```

Connect to running container

```bash
docker exec -it liferay /bin/bash
docker exec -it verdaccio /bin/sh
```

Restart a single container

```bash
docker-compose restart liferay
```

## Build & Deploy restService

```bash
cd myRestService
mvn clean install # deploys the file automatically to the container via bundle/files/deploy directory
```

## Basic Auth Configfiles

The config files are embedded in the jar, referenced via the bnd property Liferay-Configuration-Path to /src/main/resources/configuration

Docs:

- https://github.com/liferay/liferay-docs/blob/7.0.x/develop/tutorials/articles/310-troubleshooting/13-config-files.markdown
- https://dev.liferay.com/iw/discover/portal/-/knowledge_base/7-0/understanding-system-configuration-files#creating-configuration-files
- https://dev.liferay.com/iw/discover/portal/-/knowledge_base/7-0/understanding-system-configuration-files#deploying-a-configuration-file

## Liferay Requests

Call endpoint as guest

```bash
$ curl -s http://localhost:8080/o/pipiobjo/greetings

*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /o/pipiobjo/greetings HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.58.0
> Accept: */*
>
< HTTP/1.1 403
< X-Content-Type-Options: nosniff
< X-Frame-Options: SAMEORIGIN

```

Creating Token

```bash
$ echo test@liferay.com:test | base64
dGVzdEBsaWZlcmF5LmNvbTp0ZXN0Cg==
```

Check Liferay Default API

```bash
$ curl -H "Authorization: Basic dGVzdEBsaWZlcmF5LmNvbTp0ZXN0Cg==" http://localhost:8080/o/api/p/organization

```

Call our endpoint with auth token

```bash
$ curl -s -H "Authorization: Basic dGVzdEBsaWZlcmF5LmNvbTp0ZXN0Cg==" http://localhost:8080/o/pipiobjo/greetings | jq

[
  "It works!"
]

```

call via native fetch

```javascript
fetch("http://localhost:8080/o/pipiobjo/greetings", {
  method: "get",
  headers: new Headers({
    Authorization: "Basic dGVzdEBsaWZlcmF5LmNvbTp0ZXN0Cg==",
    "Content-Type": "application/json"
  })
});
```

Auth logging:
com.liferay.portal.security.auth.verifier

# UI Development

Install node and npm

```bash
$ cd ui-app
$ npm install
$ npm run serve

```

Open your browser http://localhost:9000

## Check the liferay logs during the calls

The liferay login hook is not called during this procedure, its only called if you interact on the liferay-ui

But instead you can see our CORS filter

```bash
14:10:20.095 INFO  [http-nio-8080-exec-10][CorsFilter:48] Allow cors preflight call
14:10:20.104 INFO  [http-nio-8080-exec-2][CorsFilter:58] Allow cors call
14:10:20.343 INFO  [http-nio-8080-exec-2][MyRestServiceApplication:95] remoteUser=20139 and screenName=test

```

# UI Release - Publish our JS Artefact

Apply new npm-registry

```bash
npm config set registry http://localhost:4873/
```

You can later reset it via

```bash
npm config set registry https://registry.npmjs.org/
```

Add User

```bash
$npm adduser --registry http://localhost:4873

Username: jpicado
Password: jpicado
Email: (this IS public) whatyoulike@waynetrain.com
Logged in as jpicado on http://localhost:4873/.
```

Publish artefact

```bash
cd ui-app/
$ npm publish --registry http://localhost:4873
npm notice
npm notice 📦  @pipiobjo/ui-app@0.1.0
npm notice === Tarball Contents ===
npm notice 1.4kB   package.json
npm notice 122B    .env.development
npm notice 29B     .env.production
npm notice 27B     .env.test
npm notice 53B     babel.config.js
npm notice 6.5kB   jest.config.js
npm notice 450B    README.md
npm notice 256B    vue.config.js
npm notice 384B    dist/css/app.c5fac1db.css
npm notice 1.1kB   dist/favicon.ico
npm notice 766B    dist/index.html
npm notice 455B    dist/js/about.dbab30f8.js
npm notice 1.4kB   dist/js/about.dbab30f8.js.map
npm notice 4.1kB   dist/js/app.5bf94c26.js
npm notice 23.1kB  dist/js/app.5bf94c26.js.map
npm notice 134.1kB dist/js/chunk-vendors.48f3f5cb.js
npm notice 638.6kB dist/js/chunk-vendors.48f3f5cb.js.map
npm notice 1.1kB   public/favicon.ico
npm notice 549B    public/index.html
npm notice 187B    src/api/mock/data/posts.json
npm notice 282B    src/api/mock/index.js
npm notice 351B    src/api/server-fake-auth/fakeAuth.js
npm notice 342B    src/api/server-fake-auth/index.js
npm notice 244B    src/api/server-fake-auth/users.js
npm notice 180B    src/api/server/index.js
npm notice 379B    src/App.vue
npm notice 6.8kB   src/assets/logo.png
npm notice 2.0kB   src/components/HelloWorld.vue
npm notice 213B    src/main.js
npm notice 531B    src/router.js
npm notice 387B    src/store.js
npm notice 88B     src/views/About.vue
npm notice 762B    src/views/Home.vue
npm notice 48B     tests/unit/.eslintrc.js
npm notice 377B    tests/unit/actions.spec.js
npm notice 339B    tests/unit/example.spec.js
npm notice === Tarball Details ===
npm notice name:          @pipiobjo/ui-app
npm notice version:       0.1.0
npm notice package size:  256.8 kB
npm notice unpacked size: 828.1 kB
npm notice shasum:        e592eee8f1942aade0b46df9da7bb323c468f9a5
npm notice integrity:     sha512-psmgff0rXgEHF[...]XyoqkgG+sTYPA==
npm notice total files:   36
npm notice
+ @pipiobjo/ui-app@0.1.0
```

Check the new published package in our registry
http://localhost:4873/-/web/detail/@pipiobjo/ui-app

## Migrate package resolving to our registry mirror

check content of package-lock.json, the public registry (registry.npmjs.org) is used

```json
dependencies": {
    "@babel/code-frame": {
      "version": "7.0.0",
      "resolved": "https://registry.npmjs.org/@babel/code-frame/-/code-frame-7.0.0.tgz",
      "integrity": "sha512-OfC2uemaknXr87bdLUkWog7nYuliM9Ij5HUcajsVcMCpQrcLmtxRbVFTIqmcSkSeYRBFBRxs2FiUqFJDLdiebA==",
      "dev": true,
      "requires": {
        "@babel/highlight": "^7.0.0"
      }
    },
}
```

Clear the packages, just renaming is also okay ;-)

```bash
mv node_modules node_modules.bak
mv package-lock.json package-lock.json.bak
```

and trigger a new installation

```bash
npm install
```

sample content of the new package-lock.json

```json
"dependencies": {
    "@babel/code-frame": {
      "version": "7.0.0",
      "resolved": "http://localhost:4873/@babel%2fcode-frame/-/code-frame-7.0.0.tgz",
      "integrity": "sha512-OfC2uemaknXr87bdLUkWog7nYuliM9Ij5HUcajsVcMCpQrcLmtxRbVFTIqmcSkSeYRBFBRxs2FiUqFJDLdiebA==",
      "dev": true,
      "requires": {
        "@babel/highlight": "^7.0.0"
      }
    },
}

```

# JS based UI

There are two modules:

## js-based-portlet generated via yeoman

https://github.com/liferay/liferay-npm-build-tools/wiki/How-to-use-generator-liferay-bundle


## my-npm-vue-portlet generated via maven

https://dev.liferay.com/iw/develop/reference/-/knowledge_base/7-1/npm-vue-js-portlet-template

## Requirements

In the current used version of Liferay CE the dependency of the included com.liferay.frontend.js.portlet.extender does not match to the generated code for both of the ways so we have to update it. 

- Liferay Marketplace
  - EE https://web.liferay.com/de/marketplace/-/mp/application/115543020
  - CE https://web.liferay.com/marketplace/-/mp/application/115542926
  - At time of writing both of them also including a old version
- Download it via MavenCentral https://mvnrepository.com/artifact/com.liferay/com.liferay.frontend.js.portlet.extender

  ___Its already included in the docker container setup___

For more details check the documentation of them

# Resources

* https://dev.liferay.com/iw/discover/deployment/-/knowledge_base/7-1/authentication-verifiers
* https://dev.liferay.com/develop/tutorials/-/knowledge_base/7-1/making-authenticated-requests
* https://dev.liferay.com/de/develop/tutorials/-/knowledge_base/7-1/making-authenticated-requests
* https://dev.liferay.com/de/develop/tutorials/-/knowledge_base/7-1/jax-rs

auto login
* https://github.com/liferay/liferay-blade-samples/blob/7.1/maven/extensions/auto-login/src/main/java/com/liferay/blade/samples/autologin/BladeAutoLogin.java
* https://dev.liferay.com/de/develop/tutorials/-/knowledge_base/7-1/auto-login - implementation is not working like this must extend not implement and service parameter in annotation is missing

legacy links

* https://community.liferay.com/de/forums/-/message_boards/message/87708369?_com_liferay_message_boards_web_portlet_MBPortlet_showBreadcrumb=false
