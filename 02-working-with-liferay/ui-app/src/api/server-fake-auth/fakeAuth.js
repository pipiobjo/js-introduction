import users from './users'

export default {
    /* eslint-disable */
    getAuth () {
        console.log("VUE_APP_API_CLIENT=" + process.env.VUE_APP_API_CLIENT)
        const role = process.env["VUE_APP_ROLE"]
        console.log("role=", role)
        const code = users[role].code
        console.log("code=", code)
        return code
    }

  }