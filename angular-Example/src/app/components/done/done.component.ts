import { Component, OnInit } from '@angular/core';
import { TodoState } from 'src/app/interfaces/TodoList';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

export interface AppState {
  todos: TodoState;
}

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.scss']
})
export class DoneComponent implements OnInit {

  todos$: Observable<TodoState>;

  constructor(private store: Store<AppState>) {
    this.todos$ = this.store.select('todos');
  }

  ngOnInit() {
  }

}
