import { Component, OnInit } from '@angular/core';
import { Todo } from '../../interfaces/todo';
import * as TodoActions from '../../store/todo/todo.action';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TodoState } from 'src/app/interfaces/TodoList';

export interface AppState {
  todos: TodoState;
}

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {

  todos$: Observable<TodoState>;
  todoTitle: string;
  todId: number;
  constructor(private store: Store<AppState>) {
    this.todos$ = this.store.select('todos');
    console.log(this.todos$, this.store);
  }

  ngOnInit() {
    this.todId = 0;
    this.todoTitle = '';
    this.store.dispatch(new TodoActions.GetTodos());
  }

  createTodo(): void {
    if (this.todoTitle.trim().length === 0) {
      return;
    }

    this.store.dispatch(new TodoActions.AddTodo({
      id: this.todId,
      title: this.todoTitle,
      createDate: new Date().toLocaleString(),
      doneDate: null,
      state: false,
    }));
    console.log(this.todos$);
    this.todoTitle = '';
    this.todId++;
  }
}
