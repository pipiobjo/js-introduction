import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Employee } from 'src/app/interfaces/Employee';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  employees: Array<Employee> = [];
  author: string = "";

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getEmployees();
    //this.getValueWithPromise();
    //this.waitForValueWithPromise();
    //this.addWithPromise();
    //this.addWithAsync();
   
    //this.getEmployeesAsync();

  }

  getEmployees() {
    this.apiService.getEmployees().subscribe((data: Array<Employee>) => {
      this.employees  =  data;
    });
    if(undefined !== this.employees[0]){
      this.author = this.employees[0].employee_name;
    }else{
      this.author = "Issam Lamani"
    }
  }

  async getEmployeesAsync() {
    this.employees = await this.apiService.getEmployeesAsync().toPromise();
    if(undefined !== this.employees[0]){
      this.author = this.employees[0].employee_name;
    }else{
      this.author = "Issam Lamani"
    }
  }

  // async examples
  resolveAfter2Seconds(x: any) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve(x);
      }, 2000);
    });
  }

  getValueWithPromise() {
    let a: number = 10;
    this.resolveAfter2Seconds(20).then(value => {
      // promise are {} empty opjects, i have to cast object to string
      // from string to number, to get my expected number
      a = parseInt(value.toString());
      console.log(`----> 00 promise result: ${value}`);
    });
    console.log(`----> 01 I will not wait until promise is resolved: ${a}`);
  }

  async waitForValueWithPromise() {
    let a: number = 10;
    a = <number>await this.resolveAfter2Seconds(20);
    console.log(`----> 02 I will wait until promise is resolved: ${a}`);
  }

  // waiting for multi promises example
  addWithPromise() {
    let additionPromiseResult: number = 0;
    this.resolveAfter2Seconds(20).then(data1 => {
      let result1 = <number>data1;
      this.resolveAfter2Seconds(30).then(data2 => {
        let result2 = <number>data2;
        additionPromiseResult = result1 + result2;
        console.log(`----> 03 promise result: ${additionPromiseResult}`);
      });
    });
    console.log(`----> 04 promise result: ${additionPromiseResult}`);
  }
  async addWithAsync() {
    let additionAsyncResult: number = 0;
    const result1 = <number>await this.resolveAfter2Seconds(20);
    const result2 = <number>await this.resolveAfter2Seconds(30);
    additionAsyncResult = result1 + result2;
    console.log(`----> 05 async result: ${additionAsyncResult}`);
  }
}
