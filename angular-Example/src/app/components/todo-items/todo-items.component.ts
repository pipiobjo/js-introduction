import { Component, Input, OnInit } from '@angular/core';
import { Todo } from '../../interfaces/todo';

@Component({
  selector: 'app-todo-items',
  templateUrl: './todo-items.component.html',
  styleUrls: ['./todo-items.component.scss']
})
export class TodoItemsComponent implements OnInit {

  @Input() todoItem: Todo;
  constructor() {
  }

  ngOnInit() {
  }

  taskDone(): void {
    this.todoItem.doneDate = new Date().toLocaleString();
  }

}
