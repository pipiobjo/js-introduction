import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  user = {
    name: '',
    email: '',
    age: null
  };

  constructor(private apiService: ApiService) { }

  ngOnInit() {
  }

  onSubmit(e: any) {
    console.log(e.controls.username._pendingValue);
    console.log(e.value.username, this.user.name);
    console.log(e.value.email, this.user.email);
    this.apiService.createEmployee({"name": e.value.username, "salary":"123", "age": e.value.age }).subscribe((response) => {
      console.log(response);
  });
  }
}
