import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Employee } from './interfaces/Employee';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  API_URL  =  'http://dummy.restapiexample.com/api/v1';

  constructor(private  httpClient: HttpClient) { }

  getEmployees() {
    return  this.httpClient.get(`${this.API_URL}/employees`);
  }

  getEmployeesAsync(){
    return  this.httpClient.get<Employee[]>(`${this.API_URL}/employees`);
  }

  createEmployee(data: object){
    return this.httpClient.post(`${this.API_URL}/create`,data);
  }
}
