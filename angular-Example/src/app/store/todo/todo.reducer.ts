import * as TodoActions from './todo.action';
import { Todo } from 'src/app/interfaces/todo';
import {Action} from '@ngrx/store';
import { TodoState } from 'src/app/interfaces/TodoList';

const defaultState: TodoState = {
    todos: [{
        id: 0,
        title: '',
        createDate: '',
        doneDate: '',
        state: false,
    }],
    loaded: false,
    loading: false
};

const newState = (state: TodoState, newTodo: Todo) => {
    return Object.assign({}, state, newTodo);
};

export function todoReducer(state: TodoState = defaultState, action): TodoState {
    console.log(action.type, state);
    switch (action.type) {
        case TodoActions.ADD_TODO: {
            const todos = [...state.todos, action.payload];
            return { ...state, todos};
        }
        case TodoActions.GET_TODOS:
            return { ...state, loaded: false, loading: true };
    }
    return state;
}
