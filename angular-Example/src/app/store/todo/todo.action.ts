import { Action } from '@ngrx/store';
import { Todo } from 'src/app/interfaces/todo';
import { TodoState } from 'src/app/interfaces/TodoList';


export const GET_TODOS = '[Todo] GET_TODOS';

export const ADD_TODO = '[ADD] ADD_TODO';

export class GetTodos implements Action {
    readonly type = GET_TODOS;
}

export class AddTodo implements Action {
    readonly type = ADD_TODO;

    constructor(public payload: Todo) {}
}

export type All = GetTodos | AddTodo;
