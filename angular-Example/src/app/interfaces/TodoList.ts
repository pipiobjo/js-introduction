import { Todo } from './todo';

export interface TodoState {
    todos: Todo[];
    loaded: boolean;
    loading: boolean;
}
