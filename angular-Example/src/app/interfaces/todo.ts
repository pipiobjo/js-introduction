export interface Todo {
    id: number;
    title: string;
    createDate: string;
    doneDate: string;
    state: boolean;
}
