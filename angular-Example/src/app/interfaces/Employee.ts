export interface Employee {
    id: number;
    employee_name: string;
    employee_age: number;
    employee_salary: number;
    profile_image: string;
}

export interface EmployeeConstructor {
    new (id: number, employee_name: string, employee_age: number, employee_salary: number, employee_image: string): Employee;
    clone(): Employee;
}

export var Employee: EmployeeConstructor;