import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { DoneComponent } from './components/done/done.component';
import { AboutComponent } from './components/about/about.component';
import { FormComponent } from './components/form/form.component';

const routes: Routes = [
  { path: '', redirectTo: '/todolist', pathMatch: 'full' },
  { path: 'todolist' , component: TodoListComponent },
  { path: 'done' , component: DoneComponent },
  { path: 'forms' , component: FormComponent },
  { path: 'about' , component: AboutComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
