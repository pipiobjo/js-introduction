# vue-example

## Anpassungen für Liferay 
* Unter /cor gibt es einen Script LiferayHelbers dies hilft die Instance-ID einens Portlets zu lesen. Das Script muss an zwei Stellen verwendet werden:
- App.vue --> Siehe bitte App.vue
- main.js --> Siehe bitte main.js 


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
Wenn die App für einen anderen Zielsystem gebaut werden muss, dann muss in vue.confige.js das process.env.NODE_ENV editiert werden, Beispiel:
* Apache: /vuejs --> dann muss process.env.NODE_ENV  etwa so aussehen:
```
process.env.NODE_ENV === "production" ? "/vuejs/" : "/",
```
* Liferay Portlet: Liferay Porltet haben ein osgi service Bundle-SymbolicName, dies muss als process.env.NODE_ENV in vue.confige.js verwendet werden, Beispiel:
```
process.env.NODE_ENV === "production" ? "/o/com.prodyna.demo.vuejs/" : "/",
```

und dann:

```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
