var Helpers = require("../tests/Helpers.js");

// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  "Todo Tests": browser => {
    Helpers.goToTheServer(browser);
    Helpers.navigateToDone(browser, "Done", "h1", "there is no Todos");
    Helpers.navigateToDone(browser, "Home", "input", "");
    Helpers.createTodo(browser, "Just a Test", 1);
    Helpers.createTodo(browser, "Just a Test 001", 2);
    Helpers.createTodo(browser, "Just a Test 002", 3);
    Helpers.createTodo(browser, "Just a Test 003", 4);
    Helpers.navigateToDone(browser, "Done", "div", "");
    Helpers.navigateToDone(browser, "Home", "input", "");
    Helpers.endSession(browser);
  }
};
