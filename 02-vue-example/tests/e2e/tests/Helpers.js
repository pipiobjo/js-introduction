module.exports = {
  goToTheServer: function(browser) {
    browser
      .url(process.env.VUE_DEV_SERVER_URL)
      .waitForElementVisible("#app", 5000)
      .assert.elementPresent("#nav")
      .assert.containsText("a", "Home");
    return browser;
  },

  navigateToDone: function(
    browser,
    navigationTarget,
    targetTagelement,
    containsText
  ) {
    let regEx = "//a[text()='" + navigationTarget + "']";
    browser
      .useXpath()
      .click(regEx)
      .useCss()
      .pause(1000)
      .assert.containsText(targetTagelement, containsText);
    return browser;
  },

  endSession: function(browser) {
    browser.end();
  },

  createTodo: function(browser, todoTitle, count) {
    browser
      .setValue('input[id="todoInputfield"]', todoTitle)
      .keys(browser.Keys.ENTER)
      .pause(1000)
      .assert.elementCount(".todo-items", count);
  }
};
