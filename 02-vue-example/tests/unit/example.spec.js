import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import TodoItems from "@/components/TodoItems.vue";

describe("TodoItems.vue", () => {
  it("renders TodoItems not done yet", () => {
    const wrapper = shallowMount(TodoItems, {
      propsData: {
        todoItem: {
          title: "just a test",
          state: false,
          createDate: new Date().toLocaleString()
        }
      }
    });
    expect(wrapper.text()).to.include("just a test");
    expect(wrapper.text()).to.include("Create Date");
  });
  it("renders TodoItems already done", () => {
    const wrapper = shallowMount(TodoItems, {
      propsData: {
        todoItem: {
          title: "just a test",
          state: true,
          createDate: new Date().toLocaleString()
        }
      }
    });
    expect(wrapper.text()).to.include("just a test");
    expect(wrapper.text()).to.include("Create Date");
    expect(wrapper.text()).to.include("Done at");
  });
});
