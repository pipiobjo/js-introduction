module.exports = {
  publicPath:
    process.env.NODE_ENV === "production" ? "/o/com.prodyna.demo.vuejs/" : "/",
  css: {
    extract: {
      filename: "css/[name].css",
      chunkFilename: "css/[id].css"
    }
  },
  configureWebpack: config => {
    config.output.filename = "js/[name].js";
    config.output.chunkFilename = "js/[name].js";
  }
};
