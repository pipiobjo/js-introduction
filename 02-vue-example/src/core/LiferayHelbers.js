export const getDivTarget = () => {
  var scripts = document.getElementsByTagName("script");
  var index = scripts.length - 1;
  var myScript = scripts[index];
  /* eslint-disable no-useless-escape */
  var queryString = myScript.src.replace(/^[^\?]+\??/, "");
  var liferayPortletID = queryString;
  var result = "" === liferayPortletID ? "app" : "app" + liferayPortletID;
  return result;
};
