# vuejs-portlet

## Vorbereitung
* Unter src/main/resources/META-INF muss der Inhalt des dist Verzeischnis aus der SPA eins zu eins kopiert werden, außer index.html, dies kann danach gelöscht werden
* Unter src/main/resources/META-INF gibt es zwie Dateien init.jsp und view.jsp. diese Bitte nicht ändern oder löschen.
* Unter src/main/java/com/prodyna/demo/vuejs/portlet/VueJSDemoPortlet.java müssen die css dateien hinzugefügt werden, falls neue Componente CSS mitbringen. Dies wird nicht notwendig sein, weil die CSS aus Styleguid bzw. Theme-plugin in Liferay kommen wird.

## Build
mvn clean package


