package com.prodyna.demo.vuejs.portlet;

import com.prodyna.demo.vuejs.constants.VueJSDemoPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

/**
 * @author ILamani
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=VueJS Example",
		"com.liferay.portlet.header-portlet-css=/css/about.css",
		"com.liferay.portlet.header-portlet-css=/css/app.css",
		"com.liferay.portlet.header-portlet-css=/css/done.css",
		"com.liferay.portlet.css-class-wrapper=app",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.init-param.valid-paths=/view.jsp",
		"javax.portlet.name=" + VueJSDemoPortletKeys.VueJSDemo,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user",

	},
	service = Portlet.class
)
public class VueJSDemoPortlet extends MVCPortlet {
}