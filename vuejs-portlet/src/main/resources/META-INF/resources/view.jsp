<%@ include file="/init.jsp" %>

    <noscript>
        <strong>We're sorry but vue-example doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
    </noscript>
    <div id="app<portlet:namespace/>" class="app"></div>
    <script src=/o/com.prodyna.demo.vuejs/js/chunk-vendors.js></script>
    <script src="/o/com.prodyna.demo.vuejs/js/app.js?<portlet:namespace/>"></script>