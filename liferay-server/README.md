# Docker für Liferay

Um die Beiden Porlet vuejs-portlet und angular-portlet zu vergleichen

## Start 

### First Start
Führen sie unter Linux die datei lpdocker.sh um diese Docker container zu starten
Unter Linux bitte diesen Befehl ausführen aus diesem Verzeichnis ausführen 

```
docker run -d -it --name liferay-7.1 -v $(pwd)/server:/etc/liferay/mount -p 8080:8080 liferay/portal:7.1.2-ga3 
```

### Wieder starten

```
docker start liferay-7.1 
```
